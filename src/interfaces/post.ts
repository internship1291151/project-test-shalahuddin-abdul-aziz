export interface Post {
  id: number;
  title: string;
  content: string;
  published_at: string;
  medium_image: { url: string }[];
}
