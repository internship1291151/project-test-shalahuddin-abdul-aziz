import { Parallax } from 'react-scroll-parallax';

interface Props {
  image: string;
  title: string;
  subtitle: string;
}

const Banner = ({ image, title, subtitle }: Props) => {
  return (
    <Parallax speed={-10} className="-z-10">
      <div
        className="relative h-96 w-full overflow-hidden mt-10"
        style={{ clipPath: 'polygon(0 0, 100% 0, 100% 60%, 0 100%)' }}
      >
        <img
          src={image}
          alt="Banner Image"
          className="h-full w-full object-cover brightness-75"
          style={{ transform: 'scaleX(-1)' }}
        />
        <div className="absolute inset-0 flex items-center justify-center mb-12">
          <div className="text-center text-white">
            <h1 className="text-4xl mb-2">{title}</h1>
            <p className="text-lg">{subtitle}</p>
          </div>
        </div>
      </div>
    </Parallax>
  );
};

export default Banner;
