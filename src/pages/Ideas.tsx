import React, { useCallback, useEffect, useState } from 'react';
import Header from '../components/Header';
import Banner from '../components/Banner';
import PostList from '../components/PostList';
import { Post } from '../interfaces/post';
import { ParallaxProvider } from 'react-scroll-parallax';

const Ideas = () => {
  const [posts, setPosts] = useState<Post[]>([]);
  const [total, setTotal] = useState(0);
  const [lastPage, setLastPage] = useState(1);
  const [page, setPage] = useState(1);
  const [pageSize, setPageSize] = useState<number>(
    parseInt(localStorage.getItem('selectedPageSize') as string, 10) || 10
  );
  const [sortBy, setSortBy] = useState<string>(
    localStorage.getItem('selectedSortBy') || '-published_at'
  );
  const [fetchError, setFetchError] = useState(false);

  const fetchData = useCallback(async () => {
    try {
      const response = await fetch(
        `https://suitmedia-backend.suitdev.com/api/ideas?page[number]=${page}&page[size]=${pageSize}&append[]=small_image&append[]=medium_image&sort=${sortBy}`,
        {
          method: 'GET',
          headers: {
            Accept: 'application/json',
          },
        }
      );
      const data = await response.json();
      setPosts(data.data);
      setTotal(data.meta.total);
      setLastPage(data.meta.last_page);
      setFetchError(false);
    } catch (error) {
      console.error('Error fetching data:', error);
      setFetchError(true);
    }
  }, [page, pageSize, sortBy]);

  useEffect(() => {
    fetchData();
  }, [fetchData]);

  const handleSortChange = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedSort = e.target.value;
    setSortBy(selectedSort);
    localStorage.setItem('selectedSortBy', selectedSort);
  };

  const handleNumPerPage = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const selectedNum = parseInt(e.target.value, 10);
    setPageSize(isNaN(selectedNum) ? 10 : selectedNum);
    localStorage.setItem('selectedPageSize', selectedNum.toString());
  };

  const handlePageChange = (pageNumber: number) => {
    setPage(pageNumber);
  };

  return (
    <ParallaxProvider>
      <header>
        <Header />
      </header>
      <div className="flex flex-col">
        <Banner
          image={
            'https://images.unsplash.com/photo-1507494924047-60b8ee826ca9?q=80&w=1973&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8fA%3D%3D'
          }
          title={'Ideas'}
          subtitle={'Where all our great things begin'}
        />
      </div>
      <div className="flex flex-col p-16">
        <PostList
          posts={posts}
          total={total}
          page={page}
          lastPage={lastPage}
          pageSize={pageSize}
          sortBy={sortBy}
          fetchError={fetchError}
          handleSortChange={handleSortChange}
          handleNumPerPage={handleNumPerPage}
          handlePageChange={handlePageChange}
        />
      </div>
    </ParallaxProvider>
  );
};

export default Ideas;
