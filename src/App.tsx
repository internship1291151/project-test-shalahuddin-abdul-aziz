import Ideas from './pages/Ideas';

function App() {
  return (
    <>
      <Ideas />
    </>
  );
}

export default App;
